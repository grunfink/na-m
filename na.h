/* na - A tool for asymmetric encryption of files by grunfink - public domain */

#include <stdio.h>

int na_init(void);
int na_generate_keys(const char *pk_fn, const char *sk_fn);
int na_generate_keys_from_passphrase(const char *pp, const char *pk_fn, const char *sk_fn);
int na_rebuild_public_key(const char *pk_fn, const char *sk_fn);
int na_encrypt(FILE *i, FILE *o, const char *pk_fn);
int na_decrypt(FILE *i, FILE *o, const char *sk_fn);
char *na_info(void);
char *na_version(void);
